package Loja.Modelo;

import java.util.ArrayList;
import java.util.List;

public class Unidade {

   private static int numero;
   private static String nome;
   private List<Fatura> fatura;

   public Unidade(int numero, String nome) {
      Unidade.numero = numero;
      Unidade.nome = nome;
      fatura = new ArrayList<>();
   }
 
   public static int getNumero() {
      return numero;
   }
 
   public static String getNome() {
      return nome;
   }
 
   public static List<Fatura> getFatura() {
      return fatura;
   }
 
   public static void incluirFatura (Fatura fatura) {
      fatura.add(fatura);
   }
 
   public void excluirFatura(Fatura fatura) {
      fatura.remove(fatura);
   }
}