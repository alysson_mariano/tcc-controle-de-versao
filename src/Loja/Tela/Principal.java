package Loja.Tela;

import javax.swing.JOptionPane;  //esse principal � utilizado simplesmente como tela de sa�da

import Loja.Modelo.*;

public class Principal {
 
   public static void main(String[] args) {
 
	   Unidade unidade = new Unidade(1, "S�o Paulo");
	   
       String opcaoPrincipal = "";
       do {

         opcaoPrincipal = JOptionPane.showInputDialog(null, "Cadastro de Clientes "
               + Unidade.getNumero() + " - " + Unidade.getNome() + "\n"
               + "\nOP��ES:\n"
               + "1 - Incluir cliente e codigo\n"
               + "2 - Listar clientes e suas faturas\n"
               + "3 - Sair do sistema");
 
         if (opcaoPrincipal.equals("1")) {
              Cliente cliente = new Cliente("S�o Paulo", "S�o Paulo"); 
            //aqui ele instanciou a classe cliente ou pagina Cliente.java
              cliente.setCidade(JOptionPane.showInputDialog(null, "Cidade do Cliente: "));
              cliente.setEstado(JOptionPane.showInputDialog(null, "Estado do Cliente: "));
              JOptionPane.showMessageDialog(null, "DADOS DO CLIENTE\n\n" +
              cliente.listarDados());
              
              Fatura fatura;
              String FaturaCriada = JOptionPane.showInputDialog(null, "Gerando a Fatura");
              
              fatura = new Fatura(cliente);
              
              Unidade.incluirFatura(fatura);
              
              JOptionPane.showMessageDialog(null, "DADOS DA FATURA\n\n" + Fatura.listarDados());


} else if (opcaoPrincipal.equals("2")) {


if (Unidade.getFatura().size() == 0) {

JOptionPane.showMessageDialog(null, "N�O H� CONTAS CADASTRADAS NO MOMENTO. ");

} else {

JOptionPane.showInputDialog(null, "A Loja" + Unidade.getNumero() + " - "

+ Unidade.getNome() + " possui " + Unidade.getFatura().size()

+ " faturas(s). \n\nVeja quais s�o nas pr�ximas Telas");

for (Fatura umaFatura : unidade.getFatura()) {

JOptionPane.showMessageDialog(null, umaFatura.listarDados());

}

}

} while ((opcaoPrincipal.equals("1")) || (opcaoPrincipal.equals("2")));


}

}